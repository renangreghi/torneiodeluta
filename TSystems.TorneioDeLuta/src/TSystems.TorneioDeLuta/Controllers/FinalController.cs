﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TSystems.TorneioDeLuta.Models;

namespace TSystems.TorneioDeLuta.Controllers
{
    public class FinalController : Controller
    {
        #region VARIAVEIS LUTADORES
        // Fase de Grupos
        List<LutadoresModel> faseDeGrupos = new List<LutadoresModel>();
        List<LutadoresModel> grupoA = new List<LutadoresModel>();
        List<LutadoresModel> grupoB = new List<LutadoresModel>();
        List<LutadoresModel> grupoC = new List<LutadoresModel>();
        List<LutadoresModel> grupoD = new List<LutadoresModel>();
        // Semifinal
        List<LutadoresModel> SemifinalA = new List<LutadoresModel>();
        List<LutadoresModel> SemifinalB = new List<LutadoresModel>();
        // Final
        List<LutadoresModel> Final = new List<LutadoresModel>();
        List<LutadoresModel> Terceiro = new List<LutadoresModel>();
        #endregion

        [HttpPost]
        public ActionResult Index([FromBody] LutadoresModel[] pLutadores)
        {
            faseDeGrupos = pLutadores.OrderBy(x => x.Idade).ToList();

            faseDeGrupos.ForEach(c => {
                c.CriterioVitoriaPe(c.Vitorias, c.Lutas);
            });

            int i = 0;

            foreach (LutadoresModel l in faseDeGrupos)
            {
                //Grupo A
                if (i < 5)
                {
                    grupoA.Add(l);
                }
                //Grupo B
                if (i >= 5 && i < 10)
                {
                    grupoB.Add(l);
                }
                //Grupo C
                if (i >= 10 && i < 15)
                {
                    grupoC.Add(l);
                }
                //Grupo D
                if (i >= 15 && i < 20)
                {
                    grupoD.Add(l);
                }
                i += 1;
            }
            grupoA = OrdenarLutadores(grupoA);
            grupoB = OrdenarLutadores(grupoB);
            grupoC = OrdenarLutadores(grupoC);
            grupoD = OrdenarLutadores(grupoD);
            // Implementar a logica das Quartas de Finais
            grupoA.RemoveRange(2, 3);
            grupoB.RemoveRange(2, 3);
            grupoC.RemoveRange(2, 3);
            grupoD.RemoveRange(2, 3);
            //

            if (grupoA[0].PoVitorias > grupoB[1].PoVitorias)
            {
                SemifinalA.Add(grupoA[0]);
            }
            else
            {
                SemifinalA.Add(grupoB[1]);
            }
            //
            if (grupoA[1].PoVitorias > grupoB[0].PoVitorias)
            {
                SemifinalA.Add(grupoA[1]);
            }
            else
            {
                SemifinalA.Add(grupoB[0]);
            }
            //

            if (grupoC[0].PoVitorias > grupoD[1].PoVitorias)
            {
                SemifinalB.Add(grupoC[0]);
            }
            else
            {
                SemifinalB.Add(grupoD[1]);
            }
            //
            if (grupoC[1].PoVitorias > grupoD[0].PoVitorias)
            {
                SemifinalB.Add(grupoC[1]);
            }
            else
            {
                SemifinalB.Add(grupoD[0]);
            }
            // Implementar a logica da Semifinal
            SemifinalA = OrdenarLutadores(SemifinalA);
            SemifinalB = OrdenarLutadores(SemifinalB);

            // Implementar a logica da final
            Terceiro.Add(SemifinalA[1]);
            Terceiro.Add(SemifinalB[1]);
            Terceiro = OrdenarLutadores(Terceiro);
            Terceiro.RemoveAt(1);
            Final.Add(SemifinalA[0]);
            Final.Add(SemifinalB[0]);
            Final = OrdenarLutadores(Final);
            Final.Add(Terceiro[0]);
            Final = OrdenarLutadores(Final);
            //
            TempData["campeao"] = Final[0].Nome;
            TempData["segundo"] = Final[1].Nome;
            TempData["terceiro"] = Final[2].Nome;

            return Json(new { someValue = "ok",
                              campeao = Final[0].Nome,
                              segundo = Final[1].Nome,
                              terceiro = Final[2].Nome
                            });
        }

        public ActionResult Finalizar()
        {
            string retorno = "[{\"Nome\"" + ":" + "\"" + TempData["campeao"] + "\"}," +
          "{\"Nome\"" + ":" + "\"" + TempData["segundo"] + "\"}," +
          "{\"Nome\"" + ":" + "\"" + TempData["terceiro"] + "\"}]";



            ViewBag.Final = JsonConvert.DeserializeObject(retorno);

            return View("Final");
        }
        private List<LutadoresModel> OrdenarLutadores(List<LutadoresModel> lista)
        {
            return lista.OrderByDescending(x => x.PoVitorias).ThenBy(x => x.QuantidadeArtesMarciais).ThenBy(x => x.Lutas).ToList();
        }
    }
}
