﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TSystems.TorneioDeLuta.Models;
using TSystems.TorneioDeLuta.Utils;

namespace TSystems.TorneioDeLuta.Controllers
{
    public class HomeController : Controller
    {
        public string jsonLutadores { get; set; }

        #region VARIAVEIS CONFIGURACAO/LUTADORES
        protected IConfiguration conf;
        //Inicializando lista
        List<LutadoresModel> lutadores = new List<LutadoresModel>();
        //Buscar os competidores
        Competidores comp = new Competidores();
        #endregion

        public  HomeController(IConfiguration c)
        {
            conf = c;
            var url = c.GetSection("ApiLutadores").GetSection("ApiBaseUrl").Value + c.GetSection("ApiLutadores").GetSection("MetodoPath").Value;
            this.jsonLutadores = comp.DownloadCompetidores(url.ToString());
        }

        public ActionResult Index()
        {
            if (!String.IsNullOrEmpty(jsonLutadores))
            {
                // Convertendo Json em lista
                lutadores = JsonConvert.DeserializeObject<List<LutadoresModel>>(jsonLutadores);
            }
            else
            {
                return View("Error");
            }

            lutadores.ForEach(c => {
                c.QuantidadeArtesMarciais = c.ArtesMarciais.Count;
            });
            //Comunicacao entre controller/View
            ViewBag.LutadoresModel = lutadores;
            
            return View("Index");
        }

        // Tratar Erro
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
