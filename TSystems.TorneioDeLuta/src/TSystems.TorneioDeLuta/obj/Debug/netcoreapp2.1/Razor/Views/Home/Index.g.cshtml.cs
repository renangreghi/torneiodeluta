#pragma checksum "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ddfc92f492909159fb1c16b5fd71695a6e28f7a8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\_ViewImports.cshtml"
using TSystems.TorneioDeLuta;

#line default
#line hidden
#line 2 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\_ViewImports.cshtml"
using TSystems.TorneioDeLuta.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddfc92f492909159fb1c16b5fd71695a6e28f7a8", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a1fef76234ac86b0cb573b06724be6a31fa9aa47", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TSystems.TorneioDeLuta.Models.LutadoresModel>
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(53, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
  
    ViewBag.Title = "Torneio de Luta";

#line default
#line hidden
            BeginContext(102, 232, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "630aafd1a2294067be59879c253921cf", async() => {
                BeginContext(108, 219, true);
                WriteLiteral("\r\n    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(334, 89, true);
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col-md-12\" style=\"text-align: center\">\r\n        <h1>");
            EndContext();
            BeginContext(424, 13, false);
#line 11 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
       Write(ViewBag.Title);

#line default
#line hidden
            EndContext();
            BeginContext(437, 294, true);
            WriteLiteral(@"</h1>
    </div>
</div>
<div class=""row"">
    <div class=""col-md-12"">
        <button type=""submit"" onclick=""IniciarTorneio()"" class=""btn btn-secondary"" id=""BtnIniciarFaseDeGrupo"" name=""BtnIniciarFaseDeGrupo"" value=""Index"" disabled>Iniciar</button>
    </div>
</div>
<div class=""row"">
");
            EndContext();
#line 20 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
     using (Html.BeginForm("Index", "Final", FormMethod.Post))
    {

#line default
#line hidden
            BeginContext(802, 29, true);
            WriteLiteral("    <div class=\"col-md-12\">\r\n");
            EndContext();
#line 23 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
         for (int i = 0; i < ViewBag.LutadoresModel.Count; i++)
        {

#line default
#line hidden
            BeginContext(907, 283, true);
            WriteLiteral(@"            <div class=""col-md-4"">
                <div class=""card"" style=""width: 36rem;"">
                    <div class=""card-body"">
                        <p><h4 style=""background-color:#a39f9f;""><input type=""checkbox"" name=""CbSelecionarLutador"" onclick=""SetLutador()"" /> <b>");
            EndContext();
            BeginContext(1191, 30, false);
#line 28 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
                                                                                                                                           Write(ViewBag.LutadoresModel[i].Nome);

#line default
#line hidden
            EndContext();
            BeginContext(1221, 81, true);
            WriteLiteral("</b></h4></p>\r\n                        <input type=\"hidden\" name=\"Nome\" id=\"Nome\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1302, "\"", 1341, 1);
#line 29 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
WriteAttributeValue("", 1310, ViewBag.LutadoresModel[i].Nome, 1310, 31, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1342, 73, true);
            WriteLiteral(" />\r\n                        <input type=\"hidden\" name=\"Idade\" id=\"idade\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1415, "\"", 1455, 1);
#line 30 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
WriteAttributeValue("", 1423, ViewBag.LutadoresModel[i].Idade, 1423, 32, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1456, 46, true);
            WriteLiteral(" />\r\n                        <p><b>Idade: </b>");
            EndContext();
            BeginContext(1503, 31, false);
#line 31 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
                                    Write(ViewBag.LutadoresModel[i].Idade);

#line default
#line hidden
            EndContext();
            BeginContext(1534, 74, true);
            WriteLiteral("</p>\r\n                        <input type=\"hidden\" name=\"Lutas\" id=\"Lutas\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1608, "\"", 1648, 1);
#line 32 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
WriteAttributeValue("", 1616, ViewBag.LutadoresModel[i].Lutas, 1616, 32, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1649, 46, true);
            WriteLiteral(" />\r\n                        <p><b>Lutas: </b>");
            EndContext();
            BeginContext(1696, 31, false);
#line 33 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
                                    Write(ViewBag.LutadoresModel[i].Lutas);

#line default
#line hidden
            EndContext();
            BeginContext(1727, 80, true);
            WriteLiteral("</p>\r\n                        <input type=\"hidden\" name=\"Derrotas\" id=\"Derrotas\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1807, "\"", 1850, 1);
#line 34 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
WriteAttributeValue("", 1815, ViewBag.LutadoresModel[i].Derrotas, 1815, 35, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1851, 49, true);
            WriteLiteral(" />\r\n                        <p><b>Derrotas: </b>");
            EndContext();
            BeginContext(1901, 34, false);
#line 35 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
                                       Write(ViewBag.LutadoresModel[i].Derrotas);

#line default
#line hidden
            EndContext();
            BeginContext(1935, 80, true);
            WriteLiteral("</p>\r\n                        <input type=\"hidden\" name=\"Vitorias\" id=\"Vitorias\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 2015, "\"", 2058, 1);
#line 36 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
WriteAttributeValue("", 2023, ViewBag.LutadoresModel[i].Vitorias, 2023, 35, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2059, 49, true);
            WriteLiteral(" />\r\n                        <p><b>Vitórias: </b>");
            EndContext();
            BeginContext(2109, 34, false);
#line 37 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
                                       Write(ViewBag.LutadoresModel[i].Vitorias);

#line default
#line hidden
            EndContext();
            BeginContext(2143, 110, true);
            WriteLiteral("</p>\r\n                        <input type=\"hidden\" name=\"QuantidadeArtesMarciais\" id=\"QuantidadeArtesMarciais\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 2253, "\"", 2311, 1);
#line 38 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
WriteAttributeValue("", 2261, ViewBag.LutadoresModel[i].QuantidadeArtesMarciais, 2261, 50, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2312, 55, true);
            WriteLiteral(" />\r\n                        <p><b>Artes Marciais: </b>");
            EndContext();
            BeginContext(2368, 49, false);
#line 39 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
                                             Write(ViewBag.LutadoresModel[i].QuantidadeArtesMarciais);

#line default
#line hidden
            EndContext();
            BeginContext(2417, 78, true);
            WriteLiteral("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n");
            EndContext();
#line 43 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(2510, 12, true);
            WriteLiteral("    </div>\r\n");
            EndContext();
#line 45 "C:\Renan\Projetos\TSystems - TorneioDeLuta\TSystems.TorneioDeLuta\src\TSystems.TorneioDeLuta\Views\Home\Index.cshtml"
    }

#line default
#line hidden
            BeginContext(2529, 6, true);
            WriteLiteral("</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TSystems.TorneioDeLuta.Models.LutadoresModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
