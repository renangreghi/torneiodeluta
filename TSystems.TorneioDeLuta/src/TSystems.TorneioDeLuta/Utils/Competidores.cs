﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TSystems.TorneioDeLuta.Utils
{
    public class Competidores
    {
        public string DownloadCompetidores(string url)
        {
            string jsonCompetidores = string.Empty;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = WebRequestMethods.Http.Get;
            httpWebRequest.Accept = "application/json";
            WebResponse response = httpWebRequest.GetResponse();

            using (Stream dtStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(dtStream);
                jsonCompetidores = reader.ReadToEnd();
            }
            return jsonCompetidores;
        }
    }
}
