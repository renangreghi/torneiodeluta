﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var totalLutadores = 0;
var lutadoresSelecionados = [];

function ProcessarResultado() {
    alert("Resultado processado");
}

function SetLutador() {
    totalLutadores = $('input[name="CbSelecionarLutador"]:checked').length;
    console.log("Número de lutadores:" + totalLutadores);
    
    if (totalLutadores == 20) {
        $("button[name='BtnIniciarFaseDeGrupo']").attr('class', 'btn btn-primary');
        $("button[name='BtnIniciarFaseDeGrupo']").prop("disabled", false);
    } else {
        $("button[name='BtnIniciarFaseDeGrupo']").attr('class', 'btn btn-secondary');
        $("button[name='BtnIniciarFaseDeGrupo']").prop("disabled", true);
    }
}

function IniciarTorneio() {
    if (totalLutadores == 20) {

        var checkSelecionaLutador = document.getElementsByName("CbSelecionarLutador");

        for (var i = 0; i < checkSelecionaLutador.length; i++)
        {
            if (checkSelecionaLutador[i].checked == true)
            {
                var nome = document.getElementsByName("Nome")[i].value;
                var Idade = document.getElementsByName("Idade")[i].value;
                var Lutas = document.getElementsByName("Lutas")[i].value;
                var Derrotas = document.getElementsByName("Derrotas")[i].value;
                var Vitorias = document.getElementsByName("Vitorias")[i].value;
                var QuantidadeArtesMarciais = document.getElementsByName("QuantidadeArtesMarciais")[i].value
                lutadoresSelecionados.push({ Nome: nome, Idade: Idade, Lutas: Lutas, Derrotas: Derrotas, Vitorias: Vitorias, QuantidadeArtesMarciais: QuantidadeArtesMarciais });
            }
        }        
        console.log(JSON.stringify(lutadoresSelecionados));
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'Final/Index',
            data: JSON.stringify(lutadoresSelecionados),
            dataType: 'json',
            success: function (response, data) {
                top.location.href = "Final/Finalizar";
                
            },
            error: function (err) {
            }
        });
    }
}