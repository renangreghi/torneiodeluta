﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TSystems.TorneioDeLuta.Models
{
    public class LutadoresModel
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public List<string> ArtesMarciais { get; set; }
        public int Lutas { get; set; }
        public int Derrotas { get; set; }
        public int Vitorias { get; set; }
        public int QuantidadeArtesMarciais { get; set; }

        public int PoVitorias { get; set; }

        public void CriterioVitoriaPe(double pVitorias, double pLutas)
        {
            this.PoVitorias = Convert.ToInt32(pVitorias / pLutas * 100);

        }

        public LutadoresModel() { }

    }
}
